export default {
  apiBaseUrl: "http://localhost:3000",
  title: "Parapheur",
  footerNote:
    "© Ministère de la Transition écologique et de la cohésion des territoires - 2022",
  headerTitle: "Le parapheur",
  headerSubtitle: "",
  contact: "contact-parapheur@developpement-durable.gouv.fr",
  searchEngine: true,
  unAuthentifiedEndpoints: new Set(["/auth"]),
  authenticationMode: "local",
  // casUrl: "",
  useGroups: true,
  useUnitRoles: true,
  canEscalateRoleToSelf: true,
  impersonateEnabled: true,
  fileSizeLimit: "500MiB",
  bnumUrl: "https://mel.din.developpement-durable.gouv.fr/bureau/?_task=bureau",
  exportPollInterval: 500,
  exportTimeout: 15 * 1000,
  cguLink: "http://cgu.parapheur.pnm3.eco4.cloud.e2.rie.gouv.fr/",
  accessibilityDeclarationLink:
    "http://da.parapheur.pnm3.eco4.cloud.e2.rie.gouv.fr/",
  signatures: {
    max: 15,
    extensions: ["jpeg", "jpg", "png"],
    fileSizeLimit: "5MiB",
  },
  beta: true,
  videoTutoLink:
    "https://playback.lifesize.com/#/publicvideo/582689fb-fe97-40f1-9705-4a4fbf641cd0?vcpubtoken=d5101cf1-7373-4f28-a833-bb95a7414897",
  graphicsChartLink:
    "https://intra.portail.e2.rie.gouv.fr/charte-graphique-de-l-etat-et-modeles-de-documents-a19956.html?id_rub=2341",
  goodpracticesLink:
    "http://bestpractice.parapheur.pnm3.eco4.cloud.e2.rie.gouv.fr",
  allowedConversionFormats: [
    "doc",
    "docm",
    "docx",
    "ppt",
    "pptm",
    "pptx",
    "xls",
    "xlsm",
    "xlsx",
    "odp",
    "ods",
    "odt",
  ],
  socketio: {
    transports: ["websocket"],
  },
  wopi: {
    enabled: true,
    trustedUrl: "http://192.168.1.10:9980",
    debugLocalIp: "192.168.1.10",
  },
};
