/* eslint-disable @typescript-eslint/triple-slash-reference */
/// <reference types="vitest" />
/// <reference types="vite-svg-loader" />

import { defineConfig } from "vite";
import { resolve } from "path";
import vue from "@vitejs/plugin-vue";
import svgLoader from "vite-svg-loader";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 8080,
  },
  define: {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    __APP_VERSION__: JSON.stringify(require("./package.json").version),
  },
  plugins: [vue({}), svgLoader({ defaultImport: "component" })],
  resolve: {
    dedupe: ["vue"],
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
  test: {
    globals: true,
    environment: "jsdom",
  },
});
