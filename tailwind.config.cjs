const tailwind = require("@pasta/box-v2/tailwind.config.cjs");
module.exports = {
  ...tailwind,
  content: [
    "./index.html",
    "./src/**/*.{vue,ts}",
    "./node_modules/@pasta/**/*.{vue,ts,js}",
  ],
};
