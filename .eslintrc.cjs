// eslint-disable-next-line @typescript-eslint/no-var-requires
const pastaLint = require("@pasta/lint/front");

pastaLint.extends = [
  "eslint:recommended",
  "plugin:@typescript-eslint/eslint-recommended",
  "plugin:@typescript-eslint/recommended",
  "plugin:vue/essential",
  // "@vue/typescript",
];

pastaLint.rules["vue/no-setup-props-destructure"] = "off";
pastaLint.rules["vue/require-v-for-key"] = ["warn"];
pastaLint.rules["vue/no-multiple-template-root"] = ["warn"];
pastaLint.rules["vue/no-v-model-argument"] = ["off"];
pastaLint.rules["vue/multi-word-component-names"] = ["off"];
pastaLint.rules["vue/valid-v-slot"] = ["off"];
pastaLint.rules["no-empty"] = ["warn"];

module.exports = {
  ...pastaLint,
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    sourceType: "module",
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)",
      ],
      env: {
        jest: true,
      },
    },
  ],
};
