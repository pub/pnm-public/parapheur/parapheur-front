import { useAuthStore } from "@pasta/front-auth-v2";
import { useInformationBannerStore } from "./store/InformationBanner.store";

export async function loadInformationBanner() {
  const store = useInformationBannerStore();
  const authStore = useAuthStore();
  if (authStore.isAuthenticated && !store.loaded) {
    await store.fetchActive();
  }
}
