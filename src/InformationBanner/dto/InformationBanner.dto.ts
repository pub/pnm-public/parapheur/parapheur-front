import { BaseDto, useClassFactory } from "@pasta/box-v2";
import { InformationBannerApi } from "../api/InformationBanner.api";

export enum BannerType {
  info = "info",
  error = "error",
  warning = "warning",
}

export interface InformationBannerDto extends BaseDto {
  deletedAt?: Date;
  type: BannerType;
  message: string;
  link?: string;
  linkText?: string;
}

export interface CreateInformationBannerDto {
  message: string;
  type?: BannerType;
  link?: string;
  linkText?: string;
}

export interface UpdateInformationBannerDto {
  message?: string;
  type?: BannerType;
  link?: string;
  linkText?: string;
}

export const [informationBannerApiToken, useInformationBannerApi] =
  useClassFactory(InformationBannerApi);
