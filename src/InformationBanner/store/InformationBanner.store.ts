import { Nullable, PastaCrudStoreState, crudStoreFactory } from "@pasta/box-v2";
import {
  InformationBannerDto,
  useInformationBannerApi,
} from "../dto/InformationBanner.dto";

export interface InformationBannerStoreState
  extends PastaCrudStoreState<InformationBannerDto> {
  active: Nullable<InformationBannerDto>;
  loaded: boolean;
}

export const useInformationBannerStore = crudStoreFactory(
  "informationBanner",
  useInformationBannerApi,
  (storeDef, api) => ({
    ...storeDef,
    state: (): InformationBannerStoreState => {
      return {
        ...storeDef.state(),
        active: null,
        loaded: false,
      };
    },
    actions: {
      ...storeDef.actions,
      async fetchActive() {
        this.active = await api.getActive();
        this.loaded = true;
      },
    },
  })
);
