import { Nullable, PastaCrudApi } from "@pasta/box-v2";
import {
  CreateInformationBannerDto,
  InformationBannerDto,
  UpdateInformationBannerDto,
} from "../dto/InformationBanner.dto";
import { subject } from "@casl/ability";

export class InformationBannerApi extends PastaCrudApi<{
  Entity: InformationBannerDto;
  CreateDto: CreateInformationBannerDto;
  UpdateDto: UpdateInformationBannerDto;
}> {
  protected readonly baseEndpoint = "information-banner/";

  map<T extends InformationBannerDto>(entity: T) {
    return subject("InformationBanner", entity);
  }

  async getActive(): Promise<Nullable<InformationBannerDto>> {
    return this.map(
      (await this.network.get(`${this.baseEndpoint}active`)).data
    );
  }
}
