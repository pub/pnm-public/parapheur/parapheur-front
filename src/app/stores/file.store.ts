import { defineStore } from "pinia";
import { AcceptedDto } from "../dto/file.dto";
import { useFileApi } from "../http/FileApi";

export interface FileStoreState {
  accepted: AcceptedDto;
}
export const useFileStore = defineStore("file", {
  state: (): FileStoreState => {
    return { accepted: { extensions: [], mimeTypes: [] } };
  },
  actions: {
    async getAccepted() {
      const result = await useFileApi().getAccepted();
      this.accepted = {
        extensions: result.extensions.map((item) => item.toLowerCase()),
        mimeTypes: result.mimeTypes.map((item) => item.toLowerCase()),
      };
    },
  },
});
