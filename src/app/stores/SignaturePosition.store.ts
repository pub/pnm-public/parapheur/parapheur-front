import { defineStore } from "pinia";
import {
  CreateSignaturePositionDto,
  PatchPositionsForDocDto,
  SignaturePositionDto,
  UpdateSignaturePositionDto,
} from "../dto/SignaturePosition.dto";
import { Nullable } from "@pasta/box-v2";
import { useSignaturePositionApi } from "../http/SignaturePosition.api";
import { SignCoordinates } from "../views/LetterFileForm/Sign/sign-coordinates.interface";

export interface SignaturePositionStoreState {
  currentDocumentId: Nullable<number>;
  currentLetterFileId: Nullable<number>;
  list: SignaturePositionDto[];
  map: Map<number, SignaturePositionDto>;
}

export const useSignaturePositionStore = defineStore("signature-position", {
  state: (): SignaturePositionStoreState => ({
    currentDocumentId: null,
    currentLetterFileId: null,
    list: [],
    map: new Map(),
  }),
  actions: {
    setCurrent(docId: number | null, lfId: number | null) {
      this.currentDocumentId = docId;
      this.currentLetterFileId = lfId;
    },
    setList(list: SignaturePositionDto[]) {
      this.list = list;
      this.map = new Map(list.map((item) => [item.id, item]));
    },
    async fetchCurrentDoc() {
      if (this.currentDocumentId && this.currentLetterFileId) {
        const result = await useSignaturePositionApi().getMany(
          this.currentLetterFileId,
          this.currentDocumentId
        );
        this.setList(result);
      }
    },
    async create(data: Omit<CreateSignaturePositionDto, "document">) {
      await useSignaturePositionApi().create(
        this.currentLetterFileId!,
        this.currentDocumentId!,
        { document: this.currentDocumentId!, ...data }
      );
    },
    async update(id: number, data: UpdateSignaturePositionDto) {
      await useSignaturePositionApi().update(
        id,
        this.currentLetterFileId!,
        this.currentDocumentId!,
        data
      );
    },
    async delete(id: number) {
      await useSignaturePositionApi().delete(
        id,
        this.currentLetterFileId!,
        this.currentDocumentId!
      );
    },
    async saveChanges(body: PatchPositionsForDocDto) {
      const result = await useSignaturePositionApi().patchForDoc(
        this.currentLetterFileId!,
        this.currentDocumentId!,
        body
      );

      this.setList(result);
    },
  },
});
