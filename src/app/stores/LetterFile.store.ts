import {
  CrudList,
  crudStoreFactory,
  Nullable,
  PastaCrudStoreState,
} from "@pasta/box-v2";
import { HomeDto } from "../dto/Home.dto";
import { LetterFileDto, LetterFileSearchDto } from "../dto/LetterFile.dto";
import { useLetterFileApi } from "../http/LetterFile.api";

export interface LetterFileStoreState
  extends PastaCrudStoreState<LetterFileDto> {
  chronoMap: Map<string, LetterFileDto>;
  home: HomeDto;
  currentId?: Nullable<number>;
}

export const useLetterFileStore = crudStoreFactory(
  "letterFile",
  useLetterFileApi,
  (storeDef, api) => ({
    ...storeDef,
    state: (): LetterFileStoreState => {
      return {
        ...storeDef.state(),
        chronoMap: new Map(),
        home: {
          todo: { data: [], total: 0 },
          current: { data: [], total: 0, draft: 0 },
          done: { data: [], total: 0, archived: 0 },
        },
        currentId: null,
      };
    },
    actions: {
      ...storeDef.actions,
      setList(list: CrudList<LetterFileDto>) {
        this.list = list;
        this.$patch((state) => {
          for (const element of list.data) {
            state.map.set(element.id, element);
            state.chronoMap.set(element.chronoNumber, element);
          }
        });
      },
      setOne(element: LetterFileDto) {
        this.$patch((state) => {
          state.map.set(element.id, element);
          state.chronoMap.set(element.chronoNumber, element);
        });
      },
      async fetchOneByChrono(chronoNumber: string) {
        this.setOne(await api.getByChrono(chronoNumber));
        return this.chronoMap.get(chronoNumber)!;
      },
      async fetchCurrent() {
        if (this.currentId) {
          await this.fetchOne(this.currentId);
        }
      },
      async getHome() {
        const home = await api.getHome();
        this.home = home;
      },
      async search(
        dto: LetterFileSearchDto,
        pagination: { page: number; pageSize: number }
      ) {
        const offset = (pagination.page - 1) * pagination.pageSize;
        const limit = pagination.pageSize;
        this.setList(await api.search(dto, { offset, limit }));
      },
      async undraft(id: number) {
        const result = await api.undraft(id);
        this.setOne(result);
      },
      async next(id: number) {
        const result = await api.next(id);
        this.setOne(result);
      },
      async take(id: number) {
        const result = await api.take(id);
        this.setOne(result);
      },
      async recall(id: number) {
        const result = await api.recall(id);
        this.setOne(result);
      },
      async appendDownstream(id: number, mail: string) {
        await api.appendDownstream(id, mail);
      },
      async emptyDownstream(id: number) {
        await api.emptyDownstream(id);
      },
      async reorderDownstream(id: number, downstream: number[]) {
        await api.reorderDownstream(id, downstream);
      },
      async removeDownstream(id: number, downstreamId: number) {
        await api.removeDownstream(id, downstreamId);
      },
      async reorderFolders(id: number, folders: number[]) {
        await api.reorderFolders(id, folders);
      },
      async removeAllDocs(id: number) {
        await api.removeAllDocs(id);
      },
      async applyFlux(id: number, fluxId: number) {
        await api.applyFlux(id, fluxId);
      },
      async generateArchive(id: number) {
        return api.generateArchive(id);
      },
      async clone(id: number) {
        return api.clone(id);
      },
      async unarchive(id: number) {
        return api.unarchive(id);
      },
      setCurrentId(value: number | null) {
        this.currentId = value;
      },
      getCurrent() {
        return this.currentId ? this.map.get(this.currentId) : null;
      },
    },
  })
);
