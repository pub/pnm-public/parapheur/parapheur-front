import { CrudList } from "@pasta/box-v2";
import { defineStore } from "pinia";
import { LetterFileHistoryDto } from "../dto/history.dto";
import { useLetterFileHistoryApi } from "../http/LetterFileHistory.api";

export interface LetterFileHistoryStoreState {
  list: CrudList<LetterFileHistoryDto>;
}
export const useLetterFileHistoryStore = defineStore("LetterFileHistory", {
  state: (): LetterFileHistoryStoreState => {
    return {
      list: {
        data: [],
        isLastPage: true,
        limit: 10,
        offset: 0,
        page: 1,
        total: 1,
      },
    };
  },
  actions: {
    async fetchList(
      id: number,
      pagination: { page: number; pageSize: number }
    ) {
      const offset = (pagination.page - 1) * pagination.pageSize;
      const limit = pagination.pageSize;
      const list = await useLetterFileHistoryApi().fetchList(id, {
        offset,
        limit,
      });
      this.list = list;
    },
  },
});
