import { defineStore } from "pinia";
import { useWopiApi } from "../http/Wopi.api";
import { Nullable, useConfig } from "@pasta/box-v2";
import { Dictionary } from "lodash";
import { LetterFileDocumentDto } from "../dto/LetterFileDocument.dto";
import { ParapheurConfig } from "../types";

export interface WopiStoreState {
  editablesLoaded: boolean;
  editables: Dictionary<string>;
  currentDocId?: Nullable<number>;
  currentUrl?: Nullable<string>;
  currentToken?: Nullable<string>;
  iFrameLoaded: boolean;
}

export const useWopiStore = defineStore("wopi", {
  state: (): WopiStoreState => {
    return {
      editablesLoaded: false,
      editables: {},
      currentDocId: null,
      currentUrl: null,
      currentToken: null,
      iFrameLoaded: false,
    };
  },
  actions: {
    async getToken(id: number) {
      return useWopiApi().getToken(id);
    },
    async fetchEditables() {
      if (this.config.wopi?.enabled && !this.editablesLoaded) {
        this.editables = await useWopiApi().getEditables();
        this.editablesLoaded = true;
      }
    },
    async edit(document: LetterFileDocumentDto) {
      this.currentDocId = document.id;
      this.currentUrl = `${
        this.editables[document.extension.slice(1)]
      }WOPISrc=${this.config.apiBaseUrl}/wopi/files/${
        document.id
      }&closebutton=true`;
      if (this.currentUrl.includes("localhost")) {
        if (!this.config.wopi?.debugLocalIp) {
          console.log("Please configure wopi.debugLocalIp to use locally");
          return;
        }
        // localhost hack, replace the ip here with yours when locally working with collabora
        this.currentUrl = this.currentUrl.replace(
          /localhost/g,
          this.config.wopi.debugLocalIp
        );
      }
      this.currentToken = await this.getToken(document.id);
    },
    setIFrameLoaded(value: boolean) {
      this.iFrameLoaded = value;
    },
    close() {
      this.setIFrameLoaded(false);
      this.currentDocId = null;
      this.currentToken = null;
      this.currentUrl = null;
    },
  },
  getters: {
    config() {
      return useConfig<ParapheurConfig>();
    },
  },
});
