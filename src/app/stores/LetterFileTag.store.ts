import { crudStoreFactory } from "@pasta/box-v2";
import { useLetterFileTagApi } from "../http/LetterFileTag.api";

export const useLetterFileTagStore = crudStoreFactory(
  "letterFileTag",
  useLetterFileTagApi,
  (storeDef) => storeDef
);
