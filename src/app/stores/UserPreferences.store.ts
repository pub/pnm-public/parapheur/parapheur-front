import { defineStore } from "pinia";
import { useUserPreferencesApi } from "../http/UserPreferences.api";
import {
  LetterFileColor,
  MailingFrequency,
  WriteUserPreferencesDto,
} from "../dto/UserPreferences.dto";
import { useFileApi } from "../http/FileApi";
import { NotificationType } from "../types";

export interface UserPreferencesStoreState {
  loaded: boolean;
  mailingFrequency: MailingFrequency;
  onboardingSeen: boolean;
  hideLetterFilesBefore?: string;
  signatures: string[];
  color: LetterFileColor;
  job: string;
  city: string;
  enabledMailTypes: NotificationType[];
  immediateSignature: boolean;
  lastSeenVersion: number;
}

const api = useUserPreferencesApi();
export const useUserPreferencesStore = defineStore("userPreferences", {
  state: (): UserPreferencesStoreState => ({
    loaded: false,
    mailingFrequency: MailingFrequency.REAL_TIME,
    onboardingSeen: true,
    signatures: [],
    color: LetterFileColor.GREEN,
    job: "",
    city: "",
    enabledMailTypes: Object.values(NotificationType),
    immediateSignature: false,
    lastSeenVersion: 1,
  }),
  actions: {
    setPreferences(userPrefs: Omit<UserPreferencesStoreState, "loaded">) {
      this.mailingFrequency = userPrefs.mailingFrequency;
      this.onboardingSeen = userPrefs.onboardingSeen;
      this.hideLetterFilesBefore = userPrefs.hideLetterFilesBefore;
      this.signatures = userPrefs.signatures;
      this.color = userPrefs.color;
      this.job = userPrefs.job;
      this.city = userPrefs.city;
      this.enabledMailTypes = userPrefs.enabledMailTypes;
      this.immediateSignature = userPrefs.immediateSignature;
      this.lastSeenVersion = userPrefs.lastSeenVersion;
    },
    async getMine() {
      this.setPreferences(await api.getMine());
      this.loaded = true;
    },
    async updateMine(body: WriteUserPreferencesDto) {
      this.setPreferences(await api.updateMine(body));
      this.loaded = true;
    },
  },
  getters: {
    getSignaturesLink(): Promise<string[]> {
      const fileApi = useFileApi();
      return Promise.all(
        this.signatures.map((signature) => fileApi.getLink(signature))
      );
    },
  },
});
