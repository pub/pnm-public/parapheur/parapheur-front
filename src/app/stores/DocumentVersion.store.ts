import { defineStore } from "pinia";
import { DocumentVersionDto } from "../dto/DocumentVersion.dto";
import { useLetterFileDocumentApi } from "../http/LetterFileDocument.api";

export interface DocumentVersionState {
  list: DocumentVersionDto[];
}

export const useDocumentVersionStore = defineStore("documentVersion", {
  state: (): DocumentVersionState => {
    return { list: [] };
  },
  actions: {
    async fetchForDoc(docId: number, lfId: number) {
      const result = await useLetterFileDocumentApi().getVersions(docId, lfId);
      this.list = result || [];
    },
    async getLink(id: number, docId: number, lfId: number) {
      return useLetterFileDocumentApi().getVersionLink(id, docId, lfId);
    },
  },
});
