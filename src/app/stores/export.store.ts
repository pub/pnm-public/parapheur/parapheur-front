import { defineStore } from "pinia";
import { LetterFileExportDto } from "../dto/export.dto";
import { Nullable } from "@pasta/box-v2";
import { useExportApi } from "../http/LetterFileExport.api";
import { RequestExportDto } from "../dto/LetterFile.dto";

export interface FileStoreState {
  canStartExport: boolean;
  currentExport: Nullable<LetterFileExportDto>;
}
export const useExportStore = defineStore("export", {
  state: (): FileStoreState => {
    return { currentExport: null, canStartExport: false };
  },
  actions: {
    async startExport(body: RequestExportDto) {
      const result = await useExportApi().startExport(body);
      this.currentExport = result;
      this.canStartExport = false;
      return result;
    },
    async getOne(id: number) {
      const result = await useExportApi().getOne(id);
      return result;
    },
    async poll(id: number) {
      const result = await useExportApi().poll(id);
      if (result) this.canStartExport = true;
      return result;
    },
    async fetchCanStartExport() {
      const result = await useExportApi().isReady();
      this.canStartExport = result;
      return result;
    },
    async getLink(id: number) {
      return useExportApi().getLink(id);
    },
  },
});
