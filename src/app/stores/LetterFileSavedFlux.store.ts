import { crudStoreFactory } from "@pasta/box-v2";
import { useLetterFileSavedFluxApi } from "../http/LetterFileSavedFlux.api";

export const useLetterFileSavedFluxStore = crudStoreFactory(
  "letterFileSavedFlux",
  useLetterFileSavedFluxApi,
  (storeDef, api) => {
    return {
      ...storeDef,
      actions: {
        ...storeDef.actions,
        async getMines(userId: number) {
          const result = await api.getMine(userId);
          this.$patch((state) => {
            state.list = result;
            for (const el of result.data) {
              state.map.set(el.id, el);
            }
          });
        },
      },
    };
  }
);
