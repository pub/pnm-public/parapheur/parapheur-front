import { defineStore } from "pinia";
import { EnhanceOptions } from "../dto/EnhanceSignature.dto";

export interface EnhanceSignatureStoreState {
  map: Record<string, EnhanceOptions>;
}

export const useEnhanceSignatureStore = defineStore("enhance-signature", {
  state: (): EnhanceSignatureStoreState => ({ map: {} }),
  actions: {
    setMap(map: Record<string, EnhanceOptions>) {
      this.map = map;
    },
    setOptions(key: number, optionKey: keyof EnhanceOptions, value: boolean) {
      const oldValue = this.getOptions(key);
      this.map = { ...this.map, [key]: { ...oldValue, [optionKey]: value } };
    },
  },
  getters: {
    getOptions: (state) => {
      return (key: number) => {
        return (
          state.map[key] || {
            name: false,
            date: false,
            job: false,
            city: false,
          }
        );
      };
    },
    getValue() {
      return (key: number, optionKey: keyof EnhanceOptions) => {
        return this.getOptions(key)[optionKey];
      };
    },
  },
  persist: true,
});
