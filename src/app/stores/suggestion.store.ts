import { defineStore } from "pinia";
import {
  LetterFileSuggestableField,
  SuggestionDto,
} from "../dto/Suggestion.dto";
import { useSuggestionApi } from "../http/Suggestion.api";

export interface SuggestionStoreState {
  loaded: boolean;
  list: SuggestionDto[];
  map: Map<LetterFileSuggestableField, SuggestionDto>;
}

export const useSuggestionStore = defineStore("suggestion", {
  state: (): SuggestionStoreState => {
    return { loaded: false, list: [], map: new Map() };
  },
  actions: {
    async getMy() {
      const result = await useSuggestionApi().getMy();
      result.forEach((item) => this.map.set(item.fieldType, item));
      this.list = Array.from(this.map.values());
      this.loaded = true;
    },
    async update(fieldName: LetterFileSuggestableField, values: string[]) {
      await useSuggestionApi().update(this.map.get(fieldName)!.id, { values });
    },
  },
});
