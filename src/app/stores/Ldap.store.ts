import { defineStore } from "pinia";
import { LdapAutocompleteItem } from "../dto/ldap.dto";
import { useLdapApi } from "../http/Ldap.api";

export interface LdapStoreState {
  list: LdapAutocompleteItem[];
}
export const useLdapStore = defineStore("ldap", {
  state: (): LdapStoreState => {
    return { list: [] };
  },
  actions: {
    async autocomplete(pattern: string) {
      const result = await useLdapApi().autocomplete(pattern);
      this.list = result;
    },
  },
});
