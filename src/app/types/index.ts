import { AuthConfig } from "@pasta/front-auth-v2";

export interface ParapheurConfig extends AuthConfig {
  fileSizeLimit: string;
  bnumUrl?: string;
  exportPollInterval?: number;
  exportTimeout?: number;
  cguLink: string;
  accessibilityDeclarationLink: string;
  signatures: {
    max: number;
    extensions: string[];
    fileSizeLimit: string;
  };
  beta?: boolean;
  videoTutoLink?: string;
  graphicsChartLink?: string;
  goodpracticesLink?: string;
  allowedConversionFormats: string[];
  socketio: {
    transport: string[];
  };
  wopi?: {
    enabled: boolean;
    trustedUrl: string;
    debugLocalIp?: string;
  };
}

export enum NotificationType {
  AtYourLevel = "AtYourLevel",
  CC = "CC",
  RECALL = "RECALL",
}

export interface LetterFileEventData {
  userId: number;
  userName: string;
  letterFileId: number;
}
