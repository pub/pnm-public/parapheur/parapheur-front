import { EnhanceOptions } from "../../../dto/EnhanceSignature.dto";

export enum SignType {
  DEFAULT = "default",
  PREPOS = "prepos",
}
export interface SignCoordinates {
  id?: number;
  linkedSignPositionId?: number;
  type: SignType;
  x: number;
  y: number;
  page: number;
  width: number;
  height: number;
  index?: number;
  url?: string;
  enhanceOptions?: EnhanceOptions;
  scroll?: boolean;
}
