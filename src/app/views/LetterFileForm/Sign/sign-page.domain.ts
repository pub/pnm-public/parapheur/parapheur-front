import Konva from "konva";
import { Signature } from "./signature.domain";
import { SignCoordinates, SignType } from "./sign-coordinates.interface";
import { SignaturePosition } from "./signature-position.domain";

export class SignPage {
  public readonly pageUrl: string;
  private stage: Konva.Stage;
  public layer: Konva.Layer;
  public ratio = 1;
  private kPage: Konva.Image | null = null;
  private initialized = false;

  public signatures: (Signature | SignaturePosition)[] = [];

  constructor(pageUrl: string, stage: Konva.Stage) {
    this.pageUrl = pageUrl;
    this.stage = stage;
    this.layer = new Konva.Layer();
  }

  async init() {
    if (!this.pageUrl) return;
    this.kPage = await this.getPage();
    this.layer.add(this.kPage);
    this.stage.add(this.layer);
    this.layer.hide();
    this.initialized = true;
  }

  async draw(): Promise<boolean> {
    if (this.initialized) {
      this.layer.show();
      return false;
    }
    await this.init();
    this.layer.draw();
    this.layer.show();
    return true;
  }

  async undraw() {
    this.layer.hide();
  }

  get pageX() {
    return this.kPage!.x() || 0;
  }

  get pageY() {
    return this.kPage!.y() || 0;
  }

  get pageWidth() {
    return (this.kPage!.width() || 0) * this.ratio;
  }

  get pageHeight() {
    return (this.kPage!.height() || 0) * this.ratio;
  }

  async addSignature(
    id: string,
    type: SignType,
    options: { signUrl?: string; coordinates?: SignCoordinates }
  ) {
    let signature: Signature | SignaturePosition;
    switch (type) {
      case SignType.PREPOS:
        signature = new SignaturePosition(id, this, options.coordinates!);
        break;
      case SignType.DEFAULT:
      default:
        signature = new Signature(
          id,
          this,
          options.signUrl!,
          options.coordinates
        );
        break;
    }
    this.signatures.push(signature);
    await signature.draw(options.coordinates?.scroll);

    signature.on("remove", (id) => {
      this.signatures = this.signatures.filter((s) => s.id !== id);
    });

    return signature;
  }

  async getPage(): Promise<Konva.Image> {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.src = this.pageUrl;

      image.onload = () => {
        this.ratio = this.stage.width() / image.width;
        this.stage.height(image.height * this.ratio);

        resolve(
          new Konva.Image({
            image,
            scale: {
              x: this.ratio,
              y: this.ratio,
            },
            x: (this.stage.width() - image.width * this.ratio) / 2,
            y: 0,
          })
        );
      };

      image.onerror = reject;
    });
  }
}
