import Konva from "konva";
import { SignPage } from "./sign-page.domain";
import { AbstractSignature, PADDING } from "./abstract-signature.domain";
import { unTransformXCoordinate, unTransformYCoordinate } from "./utils";

export class Signature extends AbstractSignature {
  constructor(
    public id: string,
    public signPage: SignPage,
    public signUrl: string,
    public coordinates?: { x: number; y: number; width: number; height: number }
  ) {
    super(id, signPage);
  }

  protected async getObject(): Promise<Konva.Image> {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.src = this.signUrl;

      image.onload = () => {
        if (this.coordinates && this.coordinates.width) {
          this.x = unTransformXCoordinate(
            this.coordinates.x,
            this.ratio,
            this.signPage.pageX
          );
          this.y = unTransformYCoordinate(
            this.coordinates.y,
            this.ratio,
            this.signPage.pageHeight
          );
          this.width = this.coordinates.width * this.ratio;
          this.height =
            this.coordinates.height * this.ratio ||
            (this.width * (image.height * this.ratio)) /
              (image.width * this.ratio);
        } else {
          this.width = (this.signPage.pageWidth * 20) / 100;
          this.height =
            (this.width * (image.height * this.ratio)) /
            (image.width * this.ratio);
          this.x =
            this.signPage.pageX +
            this.signPage.pageWidth -
            this.width -
            PADDING;
          this.y =
            this.signPage.pageY +
            this.signPage.pageHeight -
            this.height -
            PADDING;
        }

        const kImage = new Konva.Image({
          image,
          x: this.x,
          y: this.y,
          width: this.width,
          height: this.height,
          id: this.id,
          draggable: true,
          fillEnabled: true,
          fill: "white",
        });

        this.layer.add(kImage);
        this.layer.draw();

        resolve(kImage);
      };

      image.onerror = reject;
    });
  }
}
