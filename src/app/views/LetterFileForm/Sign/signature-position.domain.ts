import Konva from "konva";
import { AbstractSignature, PADDING } from "./abstract-signature.domain";
import { SignPage } from "./sign-page.domain";
import { SignCoordinates } from "./sign-coordinates.interface";
import { unTransformXCoordinate, unTransformYCoordinate } from "./utils";

export const SIGN_POSITION_WIDTH_RATIO = 0.7;

export class SignaturePosition extends AbstractSignature {
  constructor(id: string, signPage: SignPage, coordinates: SignCoordinates) {
    super(id, signPage);

    this.width =
      (coordinates?.width || (this.signPage.pageWidth * 20) / 100) * this.ratio;
    this.height =
      (coordinates?.height || this.width * SIGN_POSITION_WIDTH_RATIO) *
      this.ratio;

    if (!coordinates.id) {
      this.x =
        this.signPage.pageX + this.signPage.pageWidth - this.width - PADDING;
      this.y =
        this.signPage.pageY + this.signPage.pageHeight - this.height - PADDING;
    } else {
      this.x = unTransformXCoordinate(
        coordinates.x,
        this.ratio,
        this.signPage.pageX
      );
      this.y = unTransformYCoordinate(
        coordinates.y,
        this.ratio,
        this.signPage.pageHeight
      );
    }
  }

  protected async getObject(): Promise<Konva.Rect> {
    const params = {
      id: this.id,
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height,
      fill: "#fbe769",
      draggable: true,
    };
    const rect = new Konva.Rect(params);

    this.layer.add(rect);
    this.layer.draw();

    return rect;
  }
}
