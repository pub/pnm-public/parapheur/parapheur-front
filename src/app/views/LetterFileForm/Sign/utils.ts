export function transformXCoordinate(
  x: number,
  ratio: number,
  pageX: number
): number {
  return (x - pageX) / ratio;
}

export function transformYCoordinate(
  y: number,
  ratio: number,
  pageHeight: number
): number {
  return (pageHeight - y) / ratio;
}

export function unTransformXCoordinate(
  x: number,
  ratio: number,
  pageX: number
): number {
  return x * ratio + pageX;
}

export function unTransformYCoordinate(
  y: number,
  ratio: number,
  pageHeight: number
): number {
  return pageHeight - y * ratio;
}
