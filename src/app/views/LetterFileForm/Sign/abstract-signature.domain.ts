import Konva from "konva";
import { SignPage } from "./sign-page.domain";
import { EventEmitter } from "../../../utils/event-emitter.class";
import { SignCoordinates } from "./sign-coordinates.interface";
import { timeout } from "@pasta/box-v2";
import { transformXCoordinate, transformYCoordinate } from "./utils";

export const UNHIGHLIGHTED_COLOR = "#ccc";
export const UNHIGHLIGHTED_WIDTH = 1;

export const HIGHLIGHTED_COLOR = "#03098f";
export const HIGHLIGHTED_WIDTH = 2;

export const PADDING = 10;

export abstract class AbstractSignature extends EventEmitter<{
  updateCoordinates: (
    coordinates: Pick<SignCoordinates, "height" | "width" | "x" | "y">
  ) => void;
  remove: (id: string) => void;
  highlight: (id: string) => void;
}> {
  public x = 0;
  public y = 0;
  public width = 0;
  public height = 0;
  public highlighted = false;

  protected kObject: Konva.Rect | Konva.Image | null = null;
  protected kTransformer: Konva.Transformer | null = null;

  constructor(public id: string, public signPage: SignPage) {
    super();
  }

  protected abstract getObject(): Promise<Konva.Rect | Konva.Image>;

  get layer() {
    return this.signPage.layer;
  }

  get ratio() {
    return this.signPage.ratio;
  }

  get canvasEl() {
    return this.signPage.layer.getStage().container();
  }

  remove() {
    this.kObject?.remove();
    this.kTransformer?.remove();
    this.layer.draw();
    this.emit("remove", this.id);
  }

  move(x: number, y: number) {
    this.x += x;
    this.y += y;
    this.kObject!.move({ x, y });
    this.layer.draw();
    this.emitUpdate();
  }

  resize(size: number) {
    const oldWidth = this.kObject!.width();
    this.kObject!.width(this.kObject!.width() + size);
    this.kObject!.height(
      (this.kObject!.width() * this.kObject!.height()) / oldWidth
    );
    this.layer.draw();
    this.emitUpdate();
  }

  highlight() {
    this.highlighted = true;
    this.kTransformer!.setAttrs({
      borderStroke: HIGHLIGHTED_COLOR,
      borderStrokeWidth: HIGHLIGHTED_WIDTH,
      anchorStroke: HIGHLIGHTED_COLOR,
      anchorFill: HIGHLIGHTED_COLOR,
    });
    this.layer.draw();
  }

  unhighlight() {
    this.highlighted = false;
    this.kTransformer!.setAttrs({
      borderStroke: UNHIGHLIGHTED_COLOR,
      borderStrokeWidth: UNHIGHLIGHTED_WIDTH,
      anchorStroke: UNHIGHLIGHTED_COLOR,
      anchorFill: UNHIGHLIGHTED_COLOR,
    });
    this.layer.draw();
  }

  async draw(scroll = true) {
    this.kObject = await this.getObject();

    this.kTransformer = new Konva.Transformer({
      node: this.kObject,
      rotateEnabled: false,
      flipEnabled: false,
      enabledAnchors: ["top-left", "top-right", "bottom-left", "bottom-right"],
      borderStroke: UNHIGHLIGHTED_COLOR,
      borderStrokeWidth: UNHIGHLIGHTED_WIDTH,
      anchorStroke: UNHIGHLIGHTED_COLOR,
      anchorFill: UNHIGHLIGHTED_COLOR,
    });

    this.layer.add(this.kTransformer);

    this.kTransformer.on("transformend", () => {
      this.width = this.kTransformer!.width();
      this.height = this.kTransformer!.height();
      this.emitUpdate();
    });

    this.kTransformer.on("dragend", () => {
      this.x = this.kTransformer!.x();
      this.y = this.kTransformer!.y();
      this.emitUpdate();
    });

    this.kTransformer.on("dragstart", () => {
      this.emit("highlight", this.id);
    });

    this.kTransformer.on("transformstart", async () => {
      await timeout(0); // wait for the original focus event to finish
      this.emit("highlight", this.id);
    });

    this.kObject.on("click", () => {
      this.emit("highlight", this.id);
    });

    if (scroll) {
      await timeout(0);
      this.scrollTo();
    }
  }

  emitUpdate() {
    this.emit("updateCoordinates", {
      x: transformXCoordinate(this.x, this.ratio, this.signPage.pageX),
      y: transformYCoordinate(this.y, this.ratio, this.signPage.pageHeight),
      width: this.width / this.ratio,
      height: this.height / this.ratio,
    });
  }

  scrollTo() {
    this.canvasEl.scrollTo(0, this.kObject!.y());
  }
}
