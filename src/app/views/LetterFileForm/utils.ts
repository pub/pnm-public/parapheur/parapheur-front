import { isArray, isNil } from "lodash";
import { Ref } from "vue";
import { LocationQueryValue } from "vue-router";
import { LetterFileDocumentDto } from "../../dto/LetterFileDocument.dto";
import { Nullable } from "@pasta/box-v2";
import { LetterFileDto } from "../../dto/LetterFile.dto";

export const AsyncOperationFormToken = Symbol("letter-file-form-async-op");
export const FormToken = Symbol("letter-file-form");

export interface FormProvide {
  refresh: () => Promise<void>;
}

export function getBoolFromQuery(
  query: LocationQueryValue | LocationQueryValue[]
) {
  if (!query) {
    return false;
  }

  if (isArray(query)) {
    return parseBool(query[0] as string);
  }

  const value = parseBool(query);
  return value;
}

function parseBool(value: string): boolean {
  return value.toLowerCase() === "true";
}

export function getArrayStringFromQuery(
  query: LocationQueryValue | LocationQueryValue[]
): string[] {
  if (!query) {
    return [];
  }

  if (isArray(query)) {
    return query.filter((value) => !isNil(value)) as string[];
  }

  return [query];
}

export const SignToken = Symbol("sign");
export interface SignProvide {
  document: Ref<Nullable<LetterFileDocumentDto>>;
}

export function canGoNext(letterFile: LetterFileDto): boolean {
  if (letterFile?.downstream?.length === 0) {
    const ok = confirm(
      "Ce parapheur va être archivé (aucune étape dans le flux descendant), confirmer ?"
    );
    if (!ok) return false;
  }
  const hasDocs = letterFile?.folders?.find(
    (folder) => (folder.documents || []).length >= 1
  );
  if (letterFile?.comments?.length === 0 && !hasDocs) {
    const next =
      letterFile.downstream?.find((el) => el.position === 1)?.name || "Archivé";
    const ok = confirm(
      `Ce parapheur ne contient aucun document ou commentaire, êtes vous sûr(e) de vouloir l'envoyer à ${next} ?`
    );
    if (!ok) return false;
  }

  return true;
}
