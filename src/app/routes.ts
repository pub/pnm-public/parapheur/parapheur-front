import { PastaRoute, getIntFromQuery } from "@pasta/box-v2";
import { restrictedRoute } from "@pasta/front-auth-v2";
import { BiPaperclip } from "oh-vue-icons/icons";
import { RouteLocationNormalized } from "vue-router";
import { LetterFileDto, LetterFileSearchDto } from "./dto/LetterFile.dto";
import Home from "./Home.vue";
import About from "./views/About.vue";
import { loadTags } from "./router/loadTags";
import { useLetterFileStore } from "./stores/LetterFile.store";
import LetterFileForm from "./views/LetterFileForm/LetterFileForm.vue";
import { intersection, isNil } from "lodash";
import {
  getArrayStringFromQuery,
  getBoolFromQuery,
} from "./views/LetterFileForm/utils";
import { loadSavedFlux } from "./router/loadSavedFlux";
import dayjs from "dayjs";
import { loadAcceptedFiles } from "./router/loadAcceptedFiles";
import { loadUserPreferences } from "./router/loadUserPreferences";
import { useLoadingStore } from "./stores/loading.store";
import { loadInformationBanner } from "../InformationBanner/loadInformationBanner";
import FluxError from "./views/FluxError.vue";
import { joinLetterFileRoom } from "./router/joinLetterFileRoom";
import { loadVersions } from "./router/loadVersions";
import { loadWopi } from "./router/loadWopi";

export const appRoutes: PastaRoute[] = [
  {
    name: "home",
    path: "/",
    component: Home,
    beforeEnter: [
      restrictedRoute,
      loadUserPreferences,
      loadAcceptedFiles,
      loadTags,
      loadSavedFlux,
      loadInformationBanner,
      loadVersions,
      () => joinLetterFileRoom(null),
    ],
    menu: {
      label: "Accueil",
      icon: BiPaperclip,
    },
    props: (
      to: RouteLocationNormalized
    ): LetterFileSearchDto & { page: number; pageSize: number } => {
      let fileTypes: string[] = [];
      if (!isNil(to.query.fileTypes)) {
        fileTypes = Array.isArray(to.query.fileTypes)
          ? (to.query.fileTypes as string[])
          : ([to.query.fileTypes] as string[]);
      }
      return {
        page: getIntFromQuery(to.query.page, 1),
        pageSize: getIntFromQuery(to.query.pageSize, 25),
        searchPattern: to.query.searchPattern as string,
        archived: !isNil(to.query.archived)
          ? getBoolFromQuery(to.query.archived)
          : null,
        tags: getArrayStringFromQuery(to.query.tags),
        alreadySeen: getBoolFromQuery(to.query.alreadySeen),
        createdByMe: getBoolFromQuery(to.query.createdByMe),
        inTodo: getBoolFromQuery(to.query.inTodo),
        inCurrent: getBoolFromQuery(to.query.inCurrent),
        inDone: getBoolFromQuery(to.query.inDone),
        startDate: to.query.startDate as string,
        endDate: (to.query.endDate as string) || dayjs().toISOString(),
        createdStartDate: to.query.createdStartDate as string,
        createdEndDate:
          (to.query.createdEndDate as string) || dayjs().toISOString(),
        fileTypes,
      };
    },
  },
  {
    name: "form-add",
    path: "/letter-file/",
    component: LetterFileForm,
    beforeEnter: [
      restrictedRoute,
      async (_to: RouteLocationNormalized) => {
        const loadingStore = useLoadingStore();
        loadingStore.setLoading(true);
        const store = useLetterFileStore();
        const letterFile: LetterFileDto = await store.create({});
        store.setOne(letterFile);
        store.setCurrentId(letterFile.id);
        loadingStore.setLoading(false);
        return `/letter-file/${letterFile.chronoNumber}`;
      },
    ],
  },
  {
    name: "about",
    path: "/about/",
    component: About,
    beforeEnter: [
      restrictedRoute,
      loadInformationBanner,
      loadUserPreferences,
      loadVersions,
      () => joinLetterFileRoom(null),
    ],
  },
  {
    name: "form-update",
    path: "/letter-file/:chrono(\\d+)",
    component: LetterFileForm,
    beforeEnter: [
      restrictedRoute,
      loadUserPreferences,
      loadAcceptedFiles,
      loadTags,
      loadSavedFlux,
      loadInformationBanner,
      loadVersions,
      loadWopi,
      async (to: RouteLocationNormalized) => {
        const loadingStore = useLoadingStore();
        loadingStore.setLoading(true);
        const store = useLetterFileStore();
        const chrono = to.params.chrono as string;
        const lf = store.chronoMap.get(chrono);
        const joinedFields = [
          "comments",
          "folders",
          "upstream",
          "downstream",
          "ccUsers",
        ];
        if (
          !lf ||
          (lf &&
            intersection(Object.keys(lf), joinedFields).length <
              joinedFields.length)
        ) {
          try {
            const letterFile = await store.fetchOneByChrono(chrono);
            store.setCurrentId(letterFile.id);
          } catch (e: any) {
            if (e.statusCode === 404)
              return { name: "error", query: { type: "NOT_FOUND" } };
            if (e.statusCode === 403) return { name: "fluxerror" };
            return { name: "error" };
          }
        } else {
          store.setCurrentId(lf.id);
        }
        await joinLetterFileRoom(store.currentId);
        loadingStore.setLoading(false);
      },
    ],
  },
  {
    name: "fluxerror",
    path: "/fluxerror/",
    component: FluxError,
  },
];
