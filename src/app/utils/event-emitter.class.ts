export class EventEmitter<
  EventMap extends Record<string, (...args: any[]) => any>,
  EventNames extends keyof EventMap = keyof EventMap
> {
  private events: Partial<Record<EventNames, ((...args: any[]) => any)[]>> = {};

  public on<
    EventName extends EventNames,
    HandlerType extends EventMap[EventName]
  >(eventName: EventName, handler: HandlerType) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }

    this.events[eventName]!.push(handler);
    return () => {
      this.off(eventName, handler);
    };
  }

  public once<
    EventName extends EventNames,
    HandlerType extends EventMap[EventName]
  >(eventName: EventName, handler: HandlerType) {
    const onceHandler = ((...args: Parameters<HandlerType>) => {
      this.off(eventName, onceHandler);
      return handler(...args);
    }) as HandlerType;

    this.on(eventName, onceHandler);
  }

  public emit<
    EventName extends EventNames,
    HandlerType extends EventMap[EventName]
  >(eventName: EventNames, ...data: Parameters<HandlerType>) {
    const handlers = this.events[eventName];

    if (handlers) {
      handlers.forEach((fn) => {
        fn(...data);
      });
    }
  }

  public off<
    EventName extends EventNames,
    HandlerType extends EventMap[EventName]
  >(eventName: EventName, handler: HandlerType) {
    const handlers = this.events[eventName];

    if (handlers) {
      this.events[eventName] = handlers.filter((fn) => fn !== handler);
    }
  }
}
