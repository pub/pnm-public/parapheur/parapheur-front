export enum LdapEntryType {
  User = "user",
  Group = "group",
  Application = "application",
  Unknown = "unknown",
}

export interface LdapEntryDto {
  type: LdapEntryType;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  mails?: string[];
  service?: string;
  description?: string;
  gender?: "M" | "F";
  uid?: string;
  address?: string;
  phoneNumber?: string;
  relatedMails?: string[];
  users?: string[];
}

export interface LdapAutocompleteItem {
  fullName: string;
  mail: string;
  uid: string;
}
