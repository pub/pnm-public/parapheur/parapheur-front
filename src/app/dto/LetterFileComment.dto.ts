import { BaseDto } from "@pasta/box-v2";
import { UserDto } from "@pasta/front-auth-v2";
import { LetterFileDto } from "./LetterFile.dto";

export interface LetterFileCommentDto extends BaseDto {
  text: string;
  letterFile: LetterFileDto;
  author: UserDto;
  frozen: boolean;
}

export interface WriteLetterFileCommentDto {
  text: string;
  letterFile: number;
}
