export interface FileDto {
  key: string;
  originalname: string;
}

export interface AcceptedDto {
  mimeTypes: string[];
  extensions: string[];
}
