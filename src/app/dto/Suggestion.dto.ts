import { BaseDto } from "@pasta/box-v2";

export enum LetterFileSuggestableField {
  senderOrganization = "senderOrganization",
  senderName = "senderName",
  senderRole = "senderRole",
  mailType = "mailType",
  recipientName = "recipientName",
  manager = "manager",
  externalMailNumber = "externalMailNumber",
}

export interface SuggestionDto extends BaseDto {
  fieldType: LetterFileSuggestableField;
  values: string[];
}
