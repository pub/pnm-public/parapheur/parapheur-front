import { BaseDto } from "@pasta/box-v2";
import { LetterFileDto } from "./LetterFile.dto";

export interface FluxElementDto extends BaseDto {
  mail: string;
  name: string;
  position: number;
  letterFile?: LetterFileDto;
  recalled?: boolean;
}
