import { BaseDto } from "@pasta/box-v2";
import { LetterFileFolderDto } from "./LetterFileFolder.dto";
import { DocumentVersionDto } from "./DocumentVersion.dto";
import { SignaturePositionDto } from "./SignaturePosition.dto";

export enum LetterFileDocumentType {
  "DEFAULT" = "DEFAULT",
  "URL" = "URL",
}

export interface LetterFileDocumentDto extends BaseDto {
  type: LetterFileDocumentType;
  name: string;
  extension: string;
  url?: string;
  storageKey?: string;
  position: number;
  folder: LetterFileFolderDto;
  mimeType: string;
  signed?: boolean;
  nbPages?: number;
  documentVersions?: DocumentVersionDto[];
  signaturePositions?: SignaturePositionDto[];
  signaturePositionsCount?: number;
}

export interface CreateLetterFileDocumentDto {
  type?: LetterFileDocumentType;
  name: string;
  storageKey?: string;
  url?: string;
  folder: number;
}

export interface UpdateLetterFileDocumentDto {
  name?: string;
  position?: number;
  folder?: number;
}
