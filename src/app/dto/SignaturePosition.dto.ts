import { BaseDto } from "@pasta/box-v2";
import { LetterFileDocumentDto } from "./LetterFileDocument.dto";

export interface SignaturePositionDto extends BaseDto {
  x: number;
  y: number;
  width: number;
  height: number;
  page: number;
  document?: LetterFileDocumentDto;
}

export interface CreateSignaturePositionDto {
  x: number;
  y: number;
  width: number;
  height: number;
  page: number;
  document: number;
}

export interface UpdateSignaturePositionDto {
  x?: number;
  y?: number;
  width?: number;
  height?: number;
}

export type PatchPositionsForDocDto = Partial<
  Pick<SignaturePositionDto, "id" | "x" | "y" | "width" | "height">
>[];
