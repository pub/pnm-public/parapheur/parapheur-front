import { BaseDto } from "@pasta/box-v2";

export interface SavedFluxDto extends BaseDto {
  name: string;
  flux: {
    name: string;
    mail: string;
  }[];
}
