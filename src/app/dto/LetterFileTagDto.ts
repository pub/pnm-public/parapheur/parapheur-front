import { BaseDto } from "@pasta/box-v2";
import { LetterFileDto } from "./LetterFile.dto";

export interface LetterFileTagDto extends BaseDto {
  name: string;
  letterFiles?: LetterFileDto[];
}

export interface WriteLetterFileTagDto {
  name: string;
}
