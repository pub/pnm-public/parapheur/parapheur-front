import { FluxElementDto } from "./FluxElement.dto";
import { LetterFileDto } from "./LetterFile.dto";

export interface HomeDto {
  todo: { data: LetterFileHomeDto[]; total: number };
  current: { data: LetterFileHomeDto[]; total: number; draft: number };
  done: { data: LetterFileHomeDto[]; total: number; archived: number };
}

export type LetterFileHomeDto = LetterFileDto & {
  from?: FluxElementDto;
  recalled?: boolean;
};
