export interface EnhanceOptions {
  name: boolean;
  date: boolean;
  job: boolean;
  city: boolean;
}

export interface EnhanceSignatureDto extends EnhanceOptions {
  signatureIndex: number;
}
