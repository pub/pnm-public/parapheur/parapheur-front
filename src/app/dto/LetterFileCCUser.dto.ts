import { BaseDto } from "@pasta/box-v2";
import { LetterFileDto } from "./LetterFile.dto";
import { UserDto } from "@pasta/front-auth-v2";

export interface LetterFileCCUserDto extends BaseDto {
  mail: string;
  name: string;
  frozen: boolean;
  author: UserDto;
  letterFile: LetterFileDto;
}

export interface WriteLetterFileCCUserDto {
  mail: string;
  letterFile: number;
}
