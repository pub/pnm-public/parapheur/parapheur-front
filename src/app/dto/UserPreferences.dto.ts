import { BaseDto, Nullable } from "@pasta/box-v2";
import { NotificationType } from "../types";

export enum MailingFrequency {
  NONE,
  ONCE_A_DAY,
  TWICE_A_DAY,
  REAL_TIME,
}

export enum LetterFileColor {
  GREEN = "GREEN",
  BLUE = "BLUE",
  YELLOW = "YELLOW",
  RED = "RED",
}

export interface UserPreferencesDto extends BaseDto {
  login: string;
  mailingFrequency: MailingFrequency;
  onboardingSeen: boolean;
  hideLetterFilesBefore?: string;
  signatures: string[];
  color: LetterFileColor;
  job: string;
  city: string;
  enabledMailTypes: NotificationType[];
  immediateSignature: boolean;
  lastSeenVersion: number;
}

export interface WriteUserPreferencesDto {
  mailingFrequency?: MailingFrequency;
  onboardingSeen?: boolean;
  hideLetterFilesBefore?: Nullable<string>;
  signatures?: string[];
  color?: LetterFileColor;
  job?: string;
  city?: string;
  enabledMailTypes?: NotificationType[];
  immediateSignature?: boolean;
  lastSeenVersion?: number;
}
