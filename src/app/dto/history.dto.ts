import { BaseDto } from "@pasta/box-v2";

export enum HistoryOperationType {
  "CREATE" = "CREATE",
  "UPDATE" = "UPDATE",
  "DELETE" = "DELETE",
  "DOWNLOAD" = "DOWNLOAD",
}

export enum HistoryTarget {
  "DOC" = "DOC",
  "FOLDER" = "FOLDER",
  "FLUX" = "FLUX",
  "BASE-INFO" = "BASE-INFO",
  "EXTERNAL-INFO" = "EXTERNAL-INFO",
  "DEFAULT" = "DEFAULT",
  "CCUSER" = "CCUSER",
  "COMMENT" = "COMMENT",
  "SIGNATURE_POSITION" = "SIGNATURE_POSITION",
}

export interface LetterFileHistoryDto extends BaseDto {
  label: string;
  type: HistoryOperationType;
  target: HistoryTarget;
  author: string;
}
