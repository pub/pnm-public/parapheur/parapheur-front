import { BaseDto } from "@pasta/box-v2";
import { LetterFileSearchDto } from "./LetterFile.dto";

export enum ExportStatus {
  TODO = "TODO",
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED",
  ERROR = "ERROR",
}

export interface LetterFileExportDto extends BaseDto {
  fileKey: string;
  hits: number;
  status: ExportStatus;
  searchQuery: LetterFileSearchDto;
}
