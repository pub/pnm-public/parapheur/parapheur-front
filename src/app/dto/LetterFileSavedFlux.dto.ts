import { BaseDto } from "@pasta/box-v2";

export interface LetterFileSavedFluxDto extends BaseDto {
  name: string;
  flux: { name: string; mail: string }[];
}

export interface CreateLetterFileSavedFluxDto {
  name: string;
  flux: { name: string; mail: string }[];
}
