import { BaseDto } from "@pasta/box-v2";
import { LetterFileDocumentDto } from "./LetterFileDocument.dto";
import { LetterFileDto } from "./LetterFile.dto";

export interface LetterFileFolderDto extends BaseDto {
  name: string;
  position: number;
  letterFile: LetterFileDto;
  documents?: LetterFileDocumentDto[];
}

export interface CreateLetterFileFolderDto {
  name: string;
  letterFile: number;
}

export interface UpdateLetterFileFolderDto {
  name?: string;
  position?: number;
  documents?: number[];
  letterFile: number;
}
