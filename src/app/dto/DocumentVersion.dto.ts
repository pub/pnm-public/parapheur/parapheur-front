import { BaseDto } from "@pasta/box-v2";
import { LetterFileDocumentDto } from "./LetterFileDocument.dto";

export interface DocumentVersionDto extends BaseDto {
  author: string;
  storageKey: string;
  document?: LetterFileDocumentDto;
  extension?: string;
}
