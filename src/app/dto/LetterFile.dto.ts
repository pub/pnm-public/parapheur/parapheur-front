import { BaseDto, Nullable } from "@pasta/box-v2";
import { FluxElementDto } from "./FluxElement.dto";
import { LetterFileCCUserDto } from "./LetterFileCCUser.dto";
import { LetterFileCommentDto } from "./LetterFileComment.dto";
import { LetterFileFolderDto } from "./LetterFileFolder.dto";
import { LetterFileTagDto } from "./LetterFileTagDto";
import { Dictionary } from "lodash";
import { LetterFileHistoryDto } from "./history.dto";

export enum LetterFileStatus {
  UPSTREAM = "UPSTREAM",
  DOWNSTREAM = "DOWNSTREAM",
  CURRENT = "CURRENT",
  ARCHIVED = "ARCHIVED",
  UNKNOWN = "UNKNOWN",
  CC = "CC",
}

export interface LetterFileDto extends BaseDto {
  name: string;
  chronoNumber: string;
  currentPosition: number;
  deadline?: string;
  arrivalDate?: string;
  externalMailNumber?: string;
  draft: boolean;
  archived: boolean;
  unArchivedDate?: string;
  tags?: LetterFileTagDto[];
  upstream?: FluxElementDto[];
  downstream?: FluxElementDto[];
  current: Nullable<FluxElementDto>;
  ccUsers?: LetterFileCCUserDto[];
  comments?: LetterFileCommentDto[];
  folders?: LetterFileFolderDto[];
  status: LetterFileStatus;
  highlight?: Dictionary<string[]>;
  senderOrganization?: string;
  senderName?: string;
  senderRole?: string;
  mailType?: string;
  recipientName?: string;
  manager?: string;
  history?: LetterFileHistoryDto[];
}

export interface UpdateLetterFileDto {
  name?: string;
  deadline?: Date;
  arrivalDate?: Date;
  externalMailNumber?: string;
  tags?: number[];
  flux?: string[];
  folders?: number[];
}

export interface LetterFileSearchDto {
  searchPattern?: string;
  archived?: boolean | null;
  tags?: string[];
  alreadySeen?: boolean;
  createdByMe?: boolean;
  inTodo?: boolean;
  inCurrent?: boolean;
  inDone?: boolean;
  startDate?: string;
  endDate?: string;
  createdStartDate?: string;
  createdEndDate?: string;
  fileTypes?: string[];
}

export interface RequestExportDto extends LetterFileSearchDto {
  withDocuments?: boolean;
}
