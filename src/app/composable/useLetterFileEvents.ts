import { onMounted } from "vue";
import { usePastaWebsocket } from "./usePastaWebsocket";
import { LetterFileEventData } from "../types";
import { AlertDuration, AlertType, useAlertManager } from "@pasta/box-v2";
import { useAuthStore } from "@pasta/front-auth-v2";
import { useLetterFileStore } from "../stores/LetterFile.store";
import { LetterFileStatus } from "../dto/LetterFile.dto";

export function useLetterFileEvents(refresh: Function) {
  onMounted(async () => {
    const alertManager = useAlertManager();
    const authStore = useAuthStore();
    const letterFileStore = useLetterFileStore();
    const ws = usePastaWebsocket();

    await ws.on("take", "onTake", (event: LetterFileEventData) => {
      if (event.userId !== authStore.currentUser?.id) {
        alertManager.createAlert({
          type: AlertType.WARNING,
          duration: AlertDuration.INFINITE,
          message: `Le parapheur a été pris par ${event.userName}, veuillez rafraichir la page.`,
        });
      }
    });

    await ws.on("archive", "onArchive", async (event: LetterFileEventData) => {
      if (event.userId !== authStore.currentUser?.id) {
        await refresh();
        alertManager.createAlert({
          type: AlertType.WARNING,
          duration: AlertDuration.INFINITE,
          message: `Le parapheur a été archivé par ${event.userName}.`,
        });
      }
    });

    await ws.on("next", "onNext", async (event: LetterFileEventData) => {
      if (event.userId !== authStore.currentUser?.id) {
        await refresh();
        const lf = letterFileStore.getCurrent();
        const currentName = lf?.current?.name || "";
        const isCurrent = lf?.status === LetterFileStatus.CURRENT;
        const message = isCurrent
          ? "Le parapheur est arrivé à votre niveau."
          : `Le parapheur est arrivé au niveau de ${currentName}`;
        alertManager.createAlert({
          type: AlertType.WARNING,
          duration: AlertDuration.INFINITE,
          message,
        });
      }
    });
  });
}
