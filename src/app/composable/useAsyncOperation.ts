import { Deferred } from "@pasta/deferred";
import { provide, inject, computed, reactive } from "vue";

function getNotifyFn(registerOperation: AsyncOperationProvide) {
  return async function notifyAsyncOperation(fn: () => Promise<any>) {
    const deferred = registerOperation();
    try {
      await fn();
      deferred.resolve(true);
    } catch (e) {
      deferred.reject(e);
      throw e;
    }
  };
}

export function useAsyncOperation(token: Symbol) {
  const loading = computed(() => deferredMap.size > 0);
  const deferredMap = reactive<Map<number, Deferred>>(new Map());
  function registerOperation(): Deferred {
    const key = deferredMap.size;
    const deferred = new Deferred();
    deferred.finally(() => deferredMap.delete(key));
    deferredMap.set(key, deferred);
    return deferred;
  }
  provide(token, registerOperation);

  const notifyAsyncOperation = getNotifyFn(registerOperation);

  return { registerOperation, notifyAsyncOperation, loading };
}

export type AsyncOperationProvide = () => Deferred;
export function useNotifyAsyncOperation(token: Symbol) {
  const registerOperation = inject<AsyncOperationProvide>(token)!;

  return getNotifyFn(registerOperation);
}
