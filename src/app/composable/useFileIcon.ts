import { useIcons } from "@pasta/box-v2";
import {
  ViFileTypePdf2,
  ViFileTypeWord2,
  ViFileTypeText,
  ViFileTypeExcel2,
  ViFileTypeImage,
  ViFileTypeVideo,
  ViDefaultFile,
  ViFileTypePowerpoint2,
  RiFileTransferLine,
} from "oh-vue-icons/icons";
import {
  LetterFileDocumentDto,
  LetterFileDocumentType,
} from "../dto/LetterFileDocument.dto";

export function useFileIcon() {
  const [
    pdfIcon,
    wordIcon,
    textIcon,
    excelIcon,
    imageIcon,
    videoIcon,
    defaultIcon,
    diapoIcon,
    fileLinkIcon,
  ] = useIcons(
    ViFileTypePdf2,
    ViFileTypeWord2,
    ViFileTypeText,
    ViFileTypeExcel2,
    ViFileTypeImage,
    ViFileTypeVideo,
    ViDefaultFile,
    ViFileTypePowerpoint2,
    RiFileTransferLine
  );
  const iconMap = {
    [pdfIcon]: [".pdf"],
    [wordIcon]: [".docx", ".doc", ".odt"],
    [textIcon]: [".txt"],
    [excelIcon]: [".xlsx", ".xls", "xltm", ".ods"],
    [imageIcon]: [".jpeg", ".jpg", ".png"],
    [videoIcon]: [".mp4"],
    [diapoIcon]: [".pptx", ".ppt", ".odp"],
  };

  return function getIcon(doc: LetterFileDocumentDto | string) {
    if (typeof doc !== "string" && doc.type === LetterFileDocumentType.URL) {
      return fileLinkIcon;
    }

    const extension = (
      typeof doc === "string"
        ? "." + doc.split(".").at(-1)
        : doc.extension || ""
    ).toLowerCase();

    return (
      Object.entries(iconMap).find(([, v]) => v.includes(extension))?.[0] ||
      defaultIcon
    );
  };
}
