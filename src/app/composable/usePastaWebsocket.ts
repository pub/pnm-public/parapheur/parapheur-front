import { Nullable, pastaContainer, useConfig } from "@pasta/box-v2";
import { Deferred } from "@pasta/deferred";
import { useAuthStore } from "@pasta/front-auth-v2";
import io, { Socket } from "socket.io-client";
import { ParapheurConfig } from "../types";

const pastaWebsocketSymbol = Symbol("pasta-websocket");

export interface PastaSocketEvent {
  type: string;
  data: any;
}

export interface PastaSocketEventMap {
  event: (event: PastaSocketEvent) => void;
}

export class PastaWebsocket {
  private socket: Nullable<Socket<PastaSocketEventMap>> = null;
  private deferred: Deferred | null = null;
  private listeners = new Map<
    string,
    { name: string; off: Function; cb: Function }[]
  >();

  private async init() {
    if (this.socket) return;
    if (this.deferred) {
      await this.deferred;
      return;
    }
    this.deferred = new Deferred();
    this.socket = await new Promise((resolve, reject) => {
      const authStore = useAuthStore();
      if (!authStore.isAuthenticated) {
        reject(new Error("Not authenticated"));
      }

      const config = useConfig<ParapheurConfig>();
      const ws = io(config.apiBaseUrl, {
        auth: { token: authStore.accessToken },
        transports: config.socketio.transport,
      });
      ws.on("authenticated", () => {
        resolve(ws);
        this.deferred?.resolve(true);
      });
      ws.once("error", (e) => {
        reject(e);
        this.deferred?.reject(e);
      });
      ws.once("disconnect", () => {
        reject();
        this.deferred?.reject("disconnected");
      });
    });
  }

  public async emit(event: string, data: any) {
    await this.init();
    this.socket!.emit("event", { type: event, data });
  }

  /**
   *
   * @param eventType the event type to react to
   * @param uniqName  a uniq name to identify the listener
   * @param callback  the method to invoke
   * @returns
   */
  public async on(
    eventType: string,
    uniqName: string,
    callback: (data: any) => void
  ) {
    await this.init();

    const alreadyAdded = this.listeners
      .get(eventType)
      ?.some((val) => val.name === uniqName);
    if (!alreadyAdded) {
      const cb = (event: PastaSocketEvent) => {
        if (event.type === eventType) {
          callback(event.data);
        }
      };
      const off = () => {
        this.socket!.off("event", cb);
        this.listeners.set(eventType, [
          ...(this.listeners.get(eventType) || []).filter(
            (listener) => listener.name !== uniqName
          ),
        ]);
      };

      this.socket!.on("event", cb);
      this.listeners.set(eventType, [
        ...(this.listeners.get(eventType) || []),
        { name: uniqName, off, cb: callback },
      ]);

      return off;
    }
    return this.listeners
      .get(eventType)!
      .find((listener) => listener.name === uniqName)!.off;
  }
}

export function usePastaWebsocket(): PastaWebsocket {
  const websocket = pastaContainer.get(pastaWebsocketSymbol);
  if (!websocket) {
    pastaContainer.set(pastaWebsocketSymbol, new PastaWebsocket());
  }
  return pastaContainer.get(pastaWebsocketSymbol);
}
