import { computed } from "vue";
import { LetterFileColor } from "../dto/UserPreferences.dto";
import { useUserPreferencesStore } from "../stores/UserPreferences.store";

export const colorMap: Record<
  LetterFileColor,
  Record<"default" | "active" | "main", string>
> = {
  [LetterFileColor.BLUE]: {
    active: "bg-blue-cumulus-950-active",
    default: "bg-blue-cumulus-950",
    main: "#9fc3fc",
  },
  [LetterFileColor.GREEN]: {
    active: "bg-[#80d5c6]",
    default: "bg-[#bfeae2]",
    main: "#80d5c6",
  },
  [LetterFileColor.RED]: {
    active: "bg-orange-terre-battue-main-645-hover",
    default: "bg-orange-terre-battue-850",
    main: "#f1a892",
  },
  [LetterFileColor.YELLOW]: {
    active: "bg-green-tilleul-verveine-950",
    default: "bg-green-tilleul-verveine-975",
    main: "#fceeac",
  },
};

export function useLetterFileColor() {
  const userPreferencesStore = useUserPreferencesStore();
  const activeColorClass = computed(() => {
    return colorMap[userPreferencesStore.color || LetterFileColor.GREEN].active;
  });

  const defaultColorClass = computed(() => {
    return colorMap[userPreferencesStore.color || LetterFileColor.GREEN]
      .default;
  });

  const mainFeatureColor = computed(() => {
    return colorMap[userPreferencesStore.color || LetterFileColor.GREEN].main;
  });

  return { activeColorClass, defaultColorClass, mainFeatureColor };
}
