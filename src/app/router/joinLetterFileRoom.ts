import { Nullable } from "vitest";
import { usePastaWebsocket } from "../composable/usePastaWebsocket";

const ws = usePastaWebsocket();
export async function joinLetterFileRoom(id: Nullable<number>) {
  await ws.emit("viewLetterFile", id);
}
