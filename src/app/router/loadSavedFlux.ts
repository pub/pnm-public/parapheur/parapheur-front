import { useAuthStore } from "@pasta/front-auth-v2";
import { useLetterFileSavedFluxStore } from "../stores/LetterFileSavedFlux.store";

export async function loadSavedFlux() {
  const authStore = useAuthStore();
  const store = useLetterFileSavedFluxStore();
  if (authStore.isAuthenticated && !store.list.data.length) {
    await store.getMines(authStore.currentUser!.id);
  }
}
