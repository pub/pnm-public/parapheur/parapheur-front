import { useVersionsStore } from "../../Versions/Versions.store";

export async function loadVersions() {
  await useVersionsStore().fetch();
}
