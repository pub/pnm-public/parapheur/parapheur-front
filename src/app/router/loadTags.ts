import { useAuthStore } from "@pasta/front-auth-v2";
import { useLetterFileTagStore } from "../stores/LetterFileTag.store";

export async function loadTags() {
  const authStore = useAuthStore();
  const store = useLetterFileTagStore();
  if (authStore.isAuthenticated && !store.list.data.length) await store.fetchList({ page: 1, pageSize: 10 });
}
