import { useWopiStore } from "../stores/wopi.store";

export async function loadWopi() {
  await useWopiStore().fetchEditables();
}
