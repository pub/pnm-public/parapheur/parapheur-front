import { useAuthStore } from "@pasta/front-auth-v2";
import { useUserPreferencesStore } from "../stores/UserPreferences.store";

export async function loadUserPreferences() {
  const store = useUserPreferencesStore();
  const authStore = useAuthStore();
  if (authStore.isAuthenticated && !store.loaded) {
    await store.getMine();
  }
}
