import { useAuthStore } from "@pasta/front-auth-v2";
import { useFileStore } from "../stores/file.store";

export async function loadAcceptedFiles() {
  const store = useFileStore();
  const authStore = useAuthStore();
  if (
    authStore.isAuthenticated &&
    !store.accepted.extensions.length &&
    !store.accepted.mimeTypes.length
  ) {
    await store.getAccepted();
  }
}
