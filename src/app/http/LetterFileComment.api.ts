import { subject } from "@casl/ability";
import {
  NetworkClient,
  PastaApiListParams,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { omit } from "lodash";
import {
  WriteLetterFileCommentDto,
  LetterFileCommentDto,
} from "../dto/LetterFileComment.dto";

export interface ListCommentsParams extends PastaApiListParams {
  letterFileId: number;
}

export class LetterFileCommentApi {
  getBaseEndpoint(letterFileId: number) {
    return `letter-file/${letterFileId}/comments/`;
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: LetterFileCommentDto) {
    return subject("LetterFileComment", entity);
  }

  async create(dto: WriteLetterFileCommentDto): Promise<LetterFileCommentDto> {
    const response = await this.network.post(
      `${this.getBaseEndpoint(dto.letterFile)}`,
      omit(dto, "letterFile")
    );
    return this.map(response.data);
  }

  async update(
    id: number,
    dto: WriteLetterFileCommentDto
  ): Promise<LetterFileCommentDto> {
    const response = await this.network.patch(
      `${this.getBaseEndpoint(dto.letterFile)}${id}`,
      omit(dto, "letterFile")
    );
    return this.map(response.data);
  }

  async delete(id: number, letterFileId: number) {
    await this.network.delete(`${this.getBaseEndpoint(letterFileId)}${id}`);
  }
}

export const [letterFileCommentApiToken, useLetterFileCommentApi] =
  useClassFactory(LetterFileCommentApi);
