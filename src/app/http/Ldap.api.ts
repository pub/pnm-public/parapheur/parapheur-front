import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { LdapAutocompleteItem } from "../dto/ldap.dto";

export class LdapApi {
  protected readonly baseEndpoint = "/ldap";

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  async autocomplete(pattern: string): Promise<LdapAutocompleteItem[]> {
    const response = await this.network.get(`${this.baseEndpoint}`, {
      pattern,
    });
    return response.data;
  }
}

export const [ldapApiToken, useLdapApi] = useClassFactory(LdapApi);
