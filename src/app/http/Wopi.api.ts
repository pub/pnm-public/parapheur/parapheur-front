import {
  NetworkClient,
  useClassFactory,
  useConfig,
  useNetworkService,
} from "@pasta/box-v2";
import { ParapheurConfig } from "../types";
import { Dictionary } from "lodash";

export class WopiApi {
  protected readonly baseEndpoint = "wopi/";

  protected get config(): ParapheurConfig {
    return useConfig<ParapheurConfig>();
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  async getToken(documentId: number) {
    const result = await this.network.get(
      `${this.baseEndpoint}${documentId}/auth`
    );
    return result.data;
  }

  async getEditables(): Promise<Dictionary<string>> {
    const result = await this.network.get(`${this.baseEndpoint}editables`);
    return result.data;
  }
}

export const [wopiApiToken, useWopiApi] = useClassFactory(WopiApi);
