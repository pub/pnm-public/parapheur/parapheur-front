import {
  CrudList,
  NetworkClient,
  PastaApiListParams,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { LetterFileHistoryDto } from "../dto/history.dto";
import { subject } from "@casl/ability";
import { CrudRequestBuilder } from "@pasta/crud-request";

export class LetterFileHistoryApi {
  getBaseEndpoint(letterFileId: number) {
    return `letter-file/${letterFileId}/history/`;
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: LetterFileHistoryDto) {
    return subject("LetterFileHistory", entity);
  }

  async fetchList(
    lfId: number,
    params: PastaApiListParams
  ): Promise<CrudList<LetterFileHistoryDto>> {
    const qb = new CrudRequestBuilder()
      .setOffset(params.offset)
      .setLimit(params.limit);

    const { data: crudList } = await this.network.get<
      CrudList<LetterFileHistoryDto>
    >(`${this.getBaseEndpoint(lfId)}`, qb);

    crudList.data = crudList.data.map(this.map.bind(this));
    return crudList;
  }
}

export const [letterFileHistoryApiToken, useLetterFileHistoryApi] =
  useClassFactory(LetterFileHistoryApi);
