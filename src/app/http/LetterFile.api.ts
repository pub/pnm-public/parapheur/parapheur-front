import { subject } from "@casl/ability";
import {
  CrudList,
  PastaApiListParams,
  PastaCrudApi,
  useClassFactory,
} from "@pasta/box-v2";
import { CrudRequestBuilder } from "@pasta/crud-request";
import { HomeDto } from "../dto/Home.dto";
import {
  LetterFileDto,
  LetterFileSearchDto,
  UpdateLetterFileDto,
} from "../dto/LetterFile.dto";

export class LetterFileApi extends PastaCrudApi<{
  Entity: LetterFileDto;
  CreateDto: {};
  UpdateDto: UpdateLetterFileDto;
}> {
  protected readonly baseEndpoint = `letter-file/`;

  map<T extends LetterFileDto>(entity: T) {
    return subject("LetterFile", entity);
  }

  async create(): Promise<LetterFileDto> {
    const result = await this.network.post(
      `${this.baseEndpoint}`,
      {},
      {
        paramsSerializer: (qb) => qb.build(),
        params: this.getFullJoins(new CrudRequestBuilder()),
      }
    );
    return result.data;
  }

  async getByChrono(chronoNumber: string) {
    const result = await this.network.get(
      `${this.baseEndpoint}chrono/${chronoNumber}`,
      this.getFullJoins(new CrudRequestBuilder())
    );
    return this.map(result.data);
  }

  async getHome(): Promise<HomeDto> {
    const result = await this.network.get(`${this.baseEndpoint}home`);
    const homeResponse: HomeDto = result.data;
    homeResponse.todo.data = homeResponse.todo.data.map((lf) => this.map(lf));
    homeResponse.current.data = homeResponse.current.data.map((lf) =>
      this.map(lf)
    );
    homeResponse.done.data = homeResponse.done.data.map((lf) => this.map(lf));
    return result.data;
  }

  async search(
    dto: LetterFileSearchDto,
    pagination: { offset: number; limit: number }
  ): Promise<CrudList<LetterFileDto>> {
    const result = await this.network.post(`${this.baseEndpoint}search`, dto, {
      paramsSerializer: (qb) => qb.build(),
      params: new CrudRequestBuilder()
        .setOffset(pagination.offset)
        .setLimit(pagination.limit),
    });
    return result.data;
  }

  async undraft(id: number) {
    const result = await this.network.post(`${this.baseEndpoint}${id}/undraft`);
    return this.map(result.data);
  }

  async next(id: number) {
    const result = await this.network.post(`${this.baseEndpoint}${id}/next`);
    return this.map(result.data);
  }

  async take(id: number) {
    const result = await this.network.post(`${this.baseEndpoint}${id}/take`);
    return this.map(result.data);
  }

  async recall(id: number) {
    const result = await this.network.post(`${this.baseEndpoint}${id}/recall`);
    return this.map(result.data);
  }

  async appendDownstream(id: number, mail: string) {
    await this.network.post(`${this.baseEndpoint}${id}/downstream`, { mail });
  }

  async emptyDownstream(id: number) {
    await this.network.delete(`${this.baseEndpoint}${id}/downstream`);
  }

  async reorderDownstream(id: number, downstream: number[]) {
    await this.network.post(`${this.baseEndpoint}${id}/downstream/reorder`, {
      downstream,
    });
  }

  async removeDownstream(id: number, downstreamId: number) {
    await this.network.delete(
      `${this.baseEndpoint}${id}/downstream/${downstreamId}`
    );
  }

  async reorderFolders(id: number, folders: number[]) {
    await this.network.post(`${this.baseEndpoint}${id}/folders/reorder`, {
      folders,
    });
  }

  async removeAllDocs(id: number) {
    await this.network.delete(`${this.baseEndpoint}${id}/documents`);
  }

  async applyFlux(id: number, fluxId: number) {
    await this.network.post(`${this.baseEndpoint}${id}/apply-flux/${fluxId}`);
  }

  async generateArchive(id: number): Promise<string> {
    const result = await this.network.post(`${this.baseEndpoint}${id}/archive`);
    return result.data;
  }

  async clone(id: number): Promise<LetterFileDto> {
    const result = await this.network.post(`${this.baseEndpoint}${id}/clone`);
    return this.map(result.data);
  }

  async unarchive(id: number) {
    await this.network.post(`${this.baseEndpoint}${id}/unarchive`);
  }

  protected fetchOneQb(id: number, params?: undefined): CrudRequestBuilder {
    return this.getFullJoins(super.fetchOneQb(id, params));
  }

  protected getFullJoins(qb: CrudRequestBuilder) {
    return qb.addJoin(
      "folders",
      "folders.documents",
      "upstream",
      "downstream",
      "ccUsers",
      "ccUsers.author",
      "tags",
      "comments",
      "comments.author"
    );
  }

  protected fetchListQb(params: PastaApiListParams): CrudRequestBuilder {
    return super.fetchListQb(params).addJoin("tags");
  }
}

export const [letterFileApiToken, useLetterFileApi] =
  useClassFactory(LetterFileApi);
