import { CrudList, PastaCrudApi, useClassFactory } from "@pasta/box-v2";
import {
  CreateLetterFileSavedFluxDto,
  LetterFileSavedFluxDto,
} from "../dto/LetterFileSavedFlux.dto";
import { subject } from "@casl/ability";
import { CrudRequestBuilder } from "@pasta/crud-request";

export class LetterFileSavedFluxApi extends PastaCrudApi<{
  Entity: LetterFileSavedFluxDto;
  CreateDto: CreateLetterFileSavedFluxDto;
}> {
  protected readonly baseEndpoint = `saved-flux/`;

  map(entity: LetterFileSavedFluxDto) {
    return subject("LetterFileSavedFlux", entity);
  }

  async getMine(userId: number) {
    const result = await this.network.get<CrudList<LetterFileSavedFluxDto>>(
      `${this.baseEndpoint}`,
      new CrudRequestBuilder()
        .setJoin("author")
        .setFilter({ "author.id": { $eq: userId } })
    );
    return result.data;
  }
}

export const [letterFileSavedFluxApiToken, useLetterFileSavedFluxApi] =
  useClassFactory(LetterFileSavedFluxApi);
