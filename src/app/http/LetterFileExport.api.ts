import {
  NetworkClient,
  timeout,
  useClassFactory,
  useConfig,
  useNetworkService,
} from "@pasta/box-v2";
import { RequestExportDto } from "../dto/LetterFile.dto";
import { LetterFileExportDto, ExportStatus } from "../dto/export.dto";
import { subject } from "@casl/ability";
import { ParapheurConfig } from "../types";

export class ExportApi {
  protected readonly baseEndpoint = "export/";

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  protected get config(): ParapheurConfig {
    return useConfig<ParapheurConfig>();
  }

  map(entity: LetterFileExportDto) {
    return subject("LetterFileExport", entity);
  }

  async startExport(body: RequestExportDto): Promise<LetterFileExportDto> {
    const response = await this.network.post(`${this.baseEndpoint}`, body);
    return response.data;
  }

  async getOne(id: number): Promise<LetterFileExportDto> {
    const result = await this.network.get(`${this.baseEndpoint}${id}`);
    return result.data;
  }

  async poll(id: number) {
    const maxWait = this.config.exportTimeout || 3 * 60 * 1000;
    const start = Date.now();
    while (Date.now() - start < maxWait) {
      const { status, fileKey } = await this.getOne(id);
      if (status === ExportStatus.COMPLETED) return fileKey;
      await timeout(this.config.exportPollInterval || 1000);
    }
    return false;
  }

  async isReady() {
    const result = await this.network.get(`${this.baseEndpoint}is-ready`);
    return result.data;
  }

  async getLink(id: number) {
    const result = await this.network.get(`${this.baseEndpoint}${id}/link`);
    return result.data;
  }
}

export const [exportApiToken, useExportApi] = useClassFactory(ExportApi);
