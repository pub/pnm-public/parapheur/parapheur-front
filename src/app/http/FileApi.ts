import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { AcceptedDto, FileDto } from "../dto/file.dto";

export class FileApi {
  protected readonly baseEndpoint = "file/";

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  async uploadFile(form: FormData): Promise<FileDto> {
    const response = await this.network.post(`${this.baseEndpoint}`, form);
    return response.data;
  }

  async getLink(object: string) {
    const response = await this.network.get(`${this.baseEndpoint}link`, {
      object,
    });
    return response.data;
  }

  async getAccepted(): Promise<AcceptedDto> {
    const response = await this.network.get(`${this.baseEndpoint}accepted`);
    return response.data;
  }
}

export const [fileApiToken, useFileApi] = useClassFactory(FileApi);
