import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import {
  CreateLetterFileDocumentDto,
  LetterFileDocumentDto,
  UpdateLetterFileDocumentDto,
} from "../dto/LetterFileDocument.dto";
import { subject } from "@casl/ability";
import { SignCoordinates } from "../views/LetterFileForm/Sign/sign-coordinates.interface";

export class LetterFileDocumentApi {
  getBaseEndpoint(letterFileId: number) {
    return `letter-file/${letterFileId}/document/`;
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: LetterFileDocumentDto) {
    return subject("LetterFileDocument", entity);
  }

  async create(
    dto: CreateLetterFileDocumentDto,
    lfId: number
  ): Promise<LetterFileDocumentDto> {
    const response = await this.network.post(
      `${this.getBaseEndpoint(lfId)}`,
      dto
    );
    return this.map(response.data);
  }

  async update(
    id: number,
    lfId: number,
    dto: UpdateLetterFileDocumentDto
  ): Promise<LetterFileDocumentDto> {
    const response = await this.network.patch(
      `${this.getBaseEndpoint(lfId)}${id}`,
      dto
    );
    return this.map(response.data);
  }

  async delete(id: number, lfId: number) {
    await this.network.delete(`${this.getBaseEndpoint(lfId)}${id}`);
  }

  async getLink(
    id: number,
    lfId: number,
    asAttachment: boolean = false
  ): Promise<string> {
    const result = await this.network.get(
      `${this.getBaseEndpoint(lfId)}${id}/link`,
      { asAttachment }
    );
    return result.data;
  }

  async getPage(id: number, lfId: number, page: number): Promise<string> {
    const result = await this.network.get(
      `${this.getBaseEndpoint(lfId)}${id}/page/${page}`
    );
    return result.data;
  }

  async sign(
    id: number,
    lfId: number,
    signatures: SignCoordinates[]
  ): Promise<string> {
    const result = await this.network.post(
      `${this.getBaseEndpoint(lfId)}${id}/sign`,
      {
        signatures: signatures.map((signature) => ({
          x: signature.x,
          y: signature.y,
          page: signature.page,
          width: signature.width,
          height: signature.height,
          key: signature.index,
          enhanceOptions: signature.enhanceOptions,
        })),
      }
    );
    return result.data;
  }

  async getVersions(id: number, lfId: number) {
    const response = await this.network.get(
      `${this.getBaseEndpoint(lfId)}${id}/versions`
    );
    return (response.data as any[]).map((item) =>
      subject("DocumentVersion", item)
    );
  }

  async getVersionLink(vId: number, docId: number, lfId: number) {
    const result = await this.network.get(
      `${this.getBaseEndpoint(lfId)}${docId}/versions/${vId}/link`
    );
    return result.data;
  }

  async computeNbPages(id: number, lfId: number) {
    const result = await this.network.post(
      `${this.getBaseEndpoint(lfId)}${id}/computeNbPages`
    );
    return result.data;
  }
}

export const [letterFileDocumentApiToken, useLetterFileDocumentApi] =
  useClassFactory(LetterFileDocumentApi);
