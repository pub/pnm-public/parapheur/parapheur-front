import { subject } from "@casl/ability";
import { PastaCrudApi, useClassFactory } from "@pasta/box-v2";
import {
  LetterFileTagDto,
  WriteLetterFileTagDto,
} from "../dto/LetterFileTagDto";

export class LetterFileTagApi extends PastaCrudApi<{
  Entity: LetterFileTagDto;
  CreateDto: WriteLetterFileTagDto;
  UpdateDto: WriteLetterFileTagDto;
}> {
  protected readonly baseEndpoint = "letter-file-tag/";

  map(entity: LetterFileTagDto) {
    return subject("LetterFileTag", entity);
  }
}

export const [letterFileTagApiToken, useLetterFileTagApi] =
  useClassFactory(LetterFileTagApi);
