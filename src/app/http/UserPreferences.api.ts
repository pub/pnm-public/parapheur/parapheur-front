import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import {
  UserPreferencesDto,
  WriteUserPreferencesDto,
} from "../dto/UserPreferences.dto";
import { subject } from "@casl/ability";
import { EnhanceSignatureDto } from "../dto/EnhanceSignature.dto";

export class UserPreferencesApi {
  protected readonly baseEndpoint = `user-preferences/`;

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: UserPreferencesDto): UserPreferencesDto {
    return subject("UserPreferences", entity);
  }

  async getMine() {
    const response = await this.network.get(`${this.baseEndpoint}my`);
    return this.map(response.data);
  }

  async updateMine(body: WriteUserPreferencesDto) {
    const response = await this.network.patch(`${this.baseEndpoint}my`, body);
    return this.map(response.data);
  }

  async generateEnhancedSignature(
    options: EnhanceSignatureDto
  ): Promise<{ url: string }> {
    const response = await this.network.post(
      `${this.baseEndpoint}enhance-signature`,
      options
    );
    return response.data;
  }
}

export const [userPreferencesApiToken, useUserPreferencesApi] =
  useClassFactory(UserPreferencesApi);
