import { subject } from "@casl/ability";
import { PastaCrudApi, useClassFactory } from "@pasta/box-v2";
import { SuggestionDto } from "../dto/Suggestion.dto";

export class SuggestionApi extends PastaCrudApi<{
  Entity: SuggestionDto;
}> {
  protected readonly baseEndpoint = "suggestion/";

  map(entity: SuggestionDto) {
    return subject("Suggestion", entity);
  }

  async getMy() {
    const result: SuggestionDto[] = (
      await this.network.get(`${this.baseEndpoint}my`)
    ).data;
    return result.map((item) => this.map(item));
  }
}

export const [suggestionApiToken, useSuggestionApi] =
  useClassFactory(SuggestionApi);
