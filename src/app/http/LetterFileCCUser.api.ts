import { subject } from "@casl/ability";
import {
  NetworkClient,
  PastaApiListParams,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { omit } from "lodash";
import {
  LetterFileCCUserDto,
  WriteLetterFileCCUserDto,
} from "../dto/LetterFileCCUser.dto";

export interface ListCCUsersParams extends PastaApiListParams {
  letterFileId: number;
}

export class LetterFileCCUserApi {
  getBaseEndpoint(letterFileId: number) {
    return `letter-file/${letterFileId}/cc-user/`;
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: LetterFileCCUserDto) {
    return subject("LetterFileCCUser", entity);
  }

  async create(dto: WriteLetterFileCCUserDto): Promise<LetterFileCCUserDto> {
    const response = await this.network.post(
      `${this.getBaseEndpoint(dto.letterFile)}`,
      omit(dto, "letterFile")
    );
    return this.map(response.data);
  }

  async delete(id: number, letterFileId: number) {
    await this.network.delete(`${this.getBaseEndpoint(letterFileId)}${id}`);
  }

  async applyFlux(
    id: number,
    letterFileId: number
  ): Promise<{ added: number }> {
    const result = await this.network.post(
      `${this.getBaseEndpoint(letterFileId)}apply-flux/${id}`
    );
    return result.data;
  }
}

export const [letterFileCCUserApiToken, useLetterFileCCUserApi] =
  useClassFactory(LetterFileCCUserApi);
