import { subject } from "@casl/ability";
import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import {
  CreateSignaturePositionDto,
  PatchPositionsForDocDto,
  SignaturePositionDto,
  UpdateSignaturePositionDto,
} from "../dto/SignaturePosition.dto";

export class SignaturePositionApi {
  getBaseEndpoint(letterFileId: number, documentId: number) {
    return `letter-file/${letterFileId}/document/${documentId}/signature-position/`;
  }
  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: SignaturePositionDto) {
    return subject("SignaturePosition", entity);
  }

  async getMany(
    letterFileId: number,
    documentId: number
  ): Promise<SignaturePositionDto[]> {
    const response = await this.network.get(
      this.getBaseEndpoint(letterFileId, documentId)
    );
    return response.data.map(this.map);
  }

  async create(
    letterFileId: number,
    documentId: number,
    dto: CreateSignaturePositionDto
  ): Promise<SignaturePositionDto> {
    const response = await this.network.post(
      this.getBaseEndpoint(letterFileId, documentId),
      dto
    );
    return this.map(response.data);
  }

  async update(
    id: number,
    letterFileId: number,
    documentId: number,
    dto: UpdateSignaturePositionDto
  ): Promise<SignaturePositionDto> {
    const response = await this.network.patch(
      `${this.getBaseEndpoint(letterFileId, documentId)}${id}`,
      dto
    );
    return this.map(response.data);
  }

  async patchForDoc(
    letterFileId: number,
    documentId: number,
    body: PatchPositionsForDocDto
  ): Promise<SignaturePositionDto[]> {
    const response = await this.network.patch(
      `${this.getBaseEndpoint(letterFileId, documentId)}`,
      { positions: body }
    );

    return response.data.map(this.map);
  }

  async delete(
    id: number,
    letterFileId: number,
    documentId: number
  ): Promise<void> {
    await this.network.delete(
      `${this.getBaseEndpoint(letterFileId, documentId)}${id}`
    );
  }
}

export const [signaturePositionApiToken, useSignaturePositionApi] =
  useClassFactory(SignaturePositionApi);
