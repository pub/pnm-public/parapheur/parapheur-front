import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import {
  CreateLetterFileFolderDto,
  LetterFileFolderDto,
  UpdateLetterFileFolderDto,
} from "../dto/LetterFileFolder.dto";
import { omit } from "lodash";
import { subject } from "@casl/ability";

export class LetterFileFolderApi {
  getBaseEndpoint(letterFileId: number) {
    return `letter-file/${letterFileId}/folder/`;
  }

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  map(entity: LetterFileFolderDto) {
    return subject("LetterFileFolder", entity);
  }

  async create(dto: CreateLetterFileFolderDto): Promise<LetterFileFolderDto> {
    const response = await this.network.post(
      `${this.getBaseEndpoint(dto.letterFile)}`,
      omit(dto, "letterFile")
    );
    return this.map(response.data);
  }

  async update(
    id: number,
    dto: UpdateLetterFileFolderDto
  ): Promise<LetterFileFolderDto> {
    const response = await this.network.patch(
      `${this.getBaseEndpoint(dto.letterFile)}${id}`,
      omit(dto, "letterFile")
    );
    return this.map(response.data);
  }

  async delete(id: number, lfId: number) {
    await this.network.delete(`${this.getBaseEndpoint(lfId)}${id}`);
  }

  async reorder(folderId: number, lfId: number, documents: number[]) {
    await this.network.post(
      `${this.getBaseEndpoint(lfId)}${folderId}/documents/reorder`,
      { documents }
    );
  }

  async generateArchive(id: number, lfId: number): Promise<string> {
    const result = await this.network.post(
      `${this.getBaseEndpoint(lfId)}${id}/archive`
    );
    return result.data;
  }
}

export const [letterFileFolderApiToken, useLetterFileFolderApi] =
  useClassFactory(LetterFileFolderApi);
