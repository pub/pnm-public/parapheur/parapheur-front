import { computed } from "vue";
import { useUserPreferencesStore } from "../app/stores/UserPreferences.store";
import { useVersionsStore } from "./Versions.store";

export function useIsLastVersionsSeen() {
  const userPreferencesStore = useUserPreferencesStore();
  const lastSeenVersion = computed(() => userPreferencesStore.lastSeenVersion);

  const versionsStore = useVersionsStore();
  const highestVersion = computed(() => versionsStore.highestVersion?.id || 1);
  return computed(() => {
    return lastSeenVersion.value >= highestVersion.value;
  });
}
