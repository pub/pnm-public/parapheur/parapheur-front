export interface VersionDto {
  id: number;
  number: string;
  releaseDate: string;
  body: string[];
}

export type VersionsDto = VersionDto[];
