import {
  NetworkClient,
  useClassFactory,
  useNetworkService,
} from "@pasta/box-v2";
import { VersionsDto } from "./Version.dto";

export class VersionsApi {
  protected readonly baseEndpoint = "versions/";

  protected get network(): NetworkClient {
    return useNetworkService();
  }

  async getMany(): Promise<VersionsDto> {
    const response = await this.network.get(this.baseEndpoint);
    return response.data;
  }
}

export const [versionsApiToken, useVersionsApi] = useClassFactory(VersionsApi);
