import { defineStore } from "pinia";
import { VersionDto, VersionsDto } from "./Version.dto";
import { useVersionsApi } from "./Versions.api";
import { reverse, sortBy } from "lodash";

export interface VersionsStoreState {
  list: VersionsDto;
  highestVersion: VersionDto | null;
  loaded: boolean;
}

export const useVersionsStore = defineStore("versions", {
  state: (): VersionsStoreState => {
    return { list: [], loaded: false, highestVersion: null };
  },
  actions: {
    async fetch() {
      if (!this.loaded) {
        this.list = reverse(sortBy(await useVersionsApi().getMany(), "id"));
        this.highestVersion = sortBy(this.list, "id").at(-1) || null;
        this.loaded = true;
      }
    },
  },
});
