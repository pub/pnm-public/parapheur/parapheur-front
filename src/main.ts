import { loadConfig, Pasta } from "@pasta/box-v2";
import { createApp } from "vue";
import App from "./app/App.vue";
import "@pasta/box-v2/dist/style.css"; // todo: auto import
import "@pasta/front-auth-v2/dist/style.css"; // todo: auto import
import "./style.css";
import { appRoutes } from "./app/routes";
import { createPinia } from "pinia";
import piniaPluginPersistedState from "pinia-plugin-persistedstate";
import { PastaAuthBox } from "@pasta/front-auth-v2";
import CustomGenericError from "./app/views/errors/CustomGenericError.vue";
import CustomNotFoundError from "./app/views/errors/CustomNotFoundError.vue";
import CustomUnavailableError from "./app/views/errors/CustomUnavailableError.vue";
import CustomForbiddenError from "./app/views/errors/CustomForbiddenError.vue";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
dayjs.extend(utc);
dayjs.extend(timezone);

(async function main() {
  await loadConfig(__APP_VERSION__);
  // TODO: fix persisted plugin in pasta-box-v2 not working at the moment
  const pinia = createPinia().use(piniaPluginPersistedState);
  const pasta = new Pasta({
    routerOptions: { routes: [...appRoutes] },
    pinia,
    errorsMap: {
      GENERIC: CustomGenericError,
      NOT_FOUND: CustomNotFoundError,
      UNAVAILABLE: CustomUnavailableError,
      FORBIDDEN: CustomForbiddenError,
    },
  });
  createApp(App).use(pasta).use(new PastaAuthBox());
  pasta.mount("#app");
})();
